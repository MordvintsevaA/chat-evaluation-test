import React from "react";
import { RegistrationForm } from "../../components/Registration/RegistrationForm";
import UserContext from "../../contexts/UserContext"
import { withContextConsumer } from "../../contexts/utils";

const fieldsKeys = {
    login: "login",
    firstName: "firstName",
    lastName: "lastName",
    password: "password",
    confirmPassword: "confirmPassword"
}

class Registration extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            login: "",
            firstName: "",
            lastName: "",
            password: "",
            confirmPassword: "",

            onSubmit: this.onSubmit.bind(this),
            onChange: this.onChange.bind(this),
            onSignIn: this.onSignIn.bind(this)
        }
    }

    componentDidMount() {
        if (this.props.currentUser) {
            this.props.history.push("/chat")
        }
    }

    onChange(e, target) {
        e.preventDefault();
        this.setState({
            [target]: e.target.value
        })
    }

    async onSubmit(e) {
        e.preventDefault();
        const { login, firstName, lastName, password, confirmPassword } = this.state;
        this.props.register({ login, firstName, lastName, password, confirmPassword });
    }

    onSignIn(e) {
        e.preventDefault();
        this.props.history.push('');
    }

    render() {
        const {
            onChange,
            onSubmit,
            onSignIn,
            formAutocomplete,
            ...rest
        } = this.state;

        const {
            registerErrors
        } = this.props;

        return (
            <RegistrationForm
                onChange={ onChange }
                onSubmit={ onSubmit }
                onSignIn={ onSignIn }
                fieldsKeys={ fieldsKeys }
                values={ rest }
                errors={ registerErrors }
            />
        )
    }
}

export default withContextConsumer(UserContext.Consumer)(Registration);
