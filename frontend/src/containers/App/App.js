import { BrowserRouter, Route, Switch } from "react-router-dom";
import React from "react";
import Registration from "../Registration/Registration";
import Login from "../Login/Login"
import Chat from "../Chat/Chat";
import UserContext from "../../contexts/UserContext";
import ChatContext from "../../contexts/ChatContext";

function App() {
    return (
        <BrowserRouter>
            <UserContext.Provider>
                <ChatContext.Provider>
                    <Switch>
                        <Route exact path="/" component={ Login }/>
                        <Route path="/sign-up" component={ Registration }/>
                        <Route path="/chat" component={ Chat }/>
                    </Switch>
                </ChatContext.Provider>
            </UserContext.Provider>
        </BrowserRouter>
    );
}

export default App;
