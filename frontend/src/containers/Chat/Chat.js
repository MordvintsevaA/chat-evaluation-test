import React from "react";
import {withContextConsumer} from "../../contexts/utils";
import ChatContext from "../../contexts/ChatContext";
import RoomList from "../../components/Chat/RoomList/RoomList";
import styles from "./style.module.css";
import ChatWindow from "../../components/Chat/ChatWindow/ChatWindow";
import InvitationLinkModal from "../../components/InvitationLinkModal/InvitationLinkModal";
import UserContext from "../../contexts/UserContext";
import UsersList from "../../components/Chat/UsersList/UsersList";
import { filterOfflineUsers } from "../../utils/userFilter";
import ConnectionErrorModal from "../../components/ConnectionErrorModal/ConnectionErrorModal";

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeChat: null,
            activeChatMessages: [],
            activeMembers: [],

            newChatRoomName: "",
            newMessage: "",
            showInviteModal: false,
            invitationLink: "",
            connectionError: "",
            showConnectionErrorModal: false,
            showMembersList: false,

            onCreateChatClick: this.onCreateChatClick.bind(this),
            onChatRoomNameChange: this.onChatRoomNameChange.bind(this),
            onRoomSelect: this.onRoomSelect.bind(this),
            onMessageSendClick: this.onMessageSendClick.bind(this),
            onMessageChange: this.onMessageChange.bind(this),
            onInviteClick: this.onInviteClick.bind(this),
            onInvitationModalClose: this.onInvitationModalClose.bind(this),
            onMessageReceive: this.onMessageReceive.bind(this),
            onActiveMembersUpdate: this.onActiveMembersUpdate.bind(this),
            onMessageListReceive: this.onMessageListReceive.bind(this),
            onRoomDeselect: this.onRoomDeselect.bind(this),
            onConnectionError: this.onConnectionError.bind(this),
            onConnectionEstablished: this.onConnectionEstablished.bind(this),
            onConnectionErrorModalClose: this.onConnectionErrorModalClose.bind(this),
            onOpenMembersList: this.onOpenMembersList.bind(this),
            onCloseMembersList: this.onCloseMembersList.bind(this),
            onLogoutClick: this.onLogoutClick.bind(this)
        }
    }

    async componentDidMount() {
        const {
            loadRooms,
            setOnMessageReceiveCallback,
            setOnActiveMembersUpdateCallback,
            prepareConnection,
            history
        } = this.props;
        const {
            onMessageReceive,
            onActiveMembersUpdate,
            onConnectionEstablished,
            onConnectionError
        } = this.state;

        await loadRooms();
        setOnMessageReceiveCallback(onMessageReceive)
        setOnActiveMembersUpdateCallback(onActiveMembersUpdate);

        const params = new URLSearchParams(history.location.search);
        const room = params.get("room");

        if (!room) return;

        await prepareConnection(
            room,
            onConnectionEstablished,
            onConnectionError
        )
    }

    componentWillUnmount() {
        this.props.disconnectAll();
    }

    async onConnectionEstablished(room) {
        this.props.history.push({
            pathname: this.props.history.location.pathname,
            search: `?room=${room.id}`
        })

        await this.setState({
            activeChat: room,
            activeChatMessages: [],
            newMessage: "",
            showMembersList: false
        })

        this.props.getMessages(room, this.state.onMessageListReceive);
    }

    onMessageReceive(message) {
        const { activeChat } = this.state;

        if (activeChat && (message.room === activeChat.id)) {
            const updatedChatMessages = [...this.state.activeChatMessages];
            updatedChatMessages.push(message);
            this.setState({
                activeChatMessages: updatedChatMessages
            })
        }
    }

    onMessageListReceive(messages) {
        if (messages.length === 0) return;

        if (this.state.activeChat && (messages[0].room === this.state.activeChat.id)) {
            this.setState({
                activeChatMessages: messages
            })
        }
    }

    onActiveMembersUpdate(users) {
        this.setState({
            activeMembers: users
        })
    }

    async onCreateChatClick(e) {
        e.preventDefault();
        await this.props.createRoom(this.state.newChatRoomName);
        if (!this.props.createRoomError) {
            this.setState({
                newChatRoomName: ""
            })
        }
    }

    onChatRoomNameChange(e) {
        e.preventDefault();

        this.setState({
            newChatRoomName: e.target.value
        })
    }

    async onRoomSelect(e, roomId) {
        e.preventDefault();
        const { activeChat } = this.state;
        if (activeChat && (roomId === activeChat.id))
            return;
        const newActiveRoom = this.props.rooms.find(item => { return item.id === roomId });
        activeChat && (await this.props.disconnect(activeChat.id));

        await this.setState({
            activeChat: null,
            activeChatMessages: [],
            activeMembers: [],
            newMessage: "",
            showMembersList: false
        })

        await this.props.prepareConnection(
            newActiveRoom.id,
            this.state.onConnectionEstablished,
            this.state.onConnectionError
        );
    }

    async onRoomDeselect(e) {
        e.preventDefault();
        await this.props.disconnect(this.state.activeChat.id);
        this.setState({
            activeChat: null,
            activeMembers: [],
            activeChatMessages: [],
            showMembersList: false
        })
    }

    onConnectionError(data) {
        this.setState({
            connectionError: data.message,
            showConnectionErrorModal: true
        })
    }

    onMessageChange(e) {
        e.preventDefault();

        this.setState({
            newMessage: e.target.value
        })
    }

    onMessageSendClick(e) {
        e.preventDefault();
        this.props.sendMessage(this.state.activeChat, this.state.newMessage);
        this.setState({
            newMessage: ""
        })
    }

    onInviteClick(e) {
        e.preventDefault();

        this.setState({
            invitationLink: window.location.href,
            showInviteModal: true
        })
    }

    onInvitationModalClose(e) {
        e && e.preventDefault();
        this.setState({
            showInviteModal: false,
            invitationLink: ""
        })
    }

    onConnectionErrorModalClose(e) {
        e && e.preventDefault();
        this.setState({
            connectionError: "",
            showConnectionErrorModal: false
        })
    }

    onOpenMembersList(e) {
        e.preventDefault();
        this.setState({
            showMembersList: true
        })
    }

    onCloseMembersList(e) {
        e.preventDefault();
        this.setState({
            showMembersList: false
        })
    }

    onLogoutClick(e) {
        e.preventDefault();
        this.props.logout();
    }

    render() {
        const {
            newChatRoomName,
            activeChat,
            activeChatMessages,
            activeMembers,
            newMessage,
            onCreateChatClick,
            onChatRoomNameChange,
            onRoomSelect,
            onMessageSendClick,
            onMessageChange,
            onInviteClick,
            showInviteModal,
            onInvitationModalClose,
            onConnectionErrorModalClose,
            invitationLink,
            onRoomDeselect,
            connectionError,
            showConnectionErrorModal,
            showMembersList,
            onOpenMembersList,
            onCloseMembersList,
            onLogoutClick
        } = this.state;
        const {
            currentUser,
            rooms,
            sendMessageError,
            createRoomError
        } = this.props;

        return (
            <div className={ styles.container }>
                <RoomList
                    rooms={ rooms }
                    activeChatRoomId={ activeChat && activeChat.id }
                    newChatRoomName={ newChatRoomName }
                    onChatRoomNameChange={ onChatRoomNameChange }
                    onCreateChatClick={ onCreateChatClick }
                    onRoomSelect={ onRoomSelect }
                    createRoomError={ createRoomError }
                    onLogoutClick={ onLogoutClick }
                />
                {
                    activeChat && (
                        <ChatWindow
                            show={ !showMembersList }
                            activeChat={ activeChat }
                            onSendClick={ onMessageSendClick }
                            newMessage={ newMessage }
                            messages={ activeChatMessages }
                            onMessageChange={ onMessageChange }
                            sendMessageError={ sendMessageError }
                            onInviteClick={ onInviteClick }
                            currentUser={ currentUser }
                            onRoomDeselect={ onRoomDeselect }
                            onOpenMembersList={ onOpenMembersList }
                        />
                    )
                }
                {
                    activeChat && (
                        <UsersList
                            offline={ filterOfflineUsers(activeChat.users, activeMembers) }
                            online={ activeMembers }
                            open={ showMembersList }
                            onClose={ onCloseMembersList }
                        />
                    )
                }
                <InvitationLinkModal
                    open={ showInviteModal }
                    onClose={ onInvitationModalClose }
                    invitationLink={ invitationLink }
                />
                {
                    showConnectionErrorModal && (
                        <ConnectionErrorModal
                            open={ showConnectionErrorModal }
                            errorMessage={ connectionError }
                            onClose={ onConnectionErrorModalClose }
                        />
                    )
                }
            </div>
        )
    }
}

export default withContextConsumer(UserContext.Consumer)(withContextConsumer(ChatContext.Consumer)(Chat));
