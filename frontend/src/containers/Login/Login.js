import React from "react";
import { LoginForm } from "../../components/Login/LoginForm";
import { withContextConsumer } from "../../contexts/utils";
import UserContext from "../../contexts/UserContext";

const fieldsKeys = {
    login: "login",
    password: "password"
}

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: "",
            password: "",

            onChangeField: this.onChangeField.bind(this),
            onSubmit: this.onSubmit.bind(this),
            onSignUp: this.onSignUp.bind(this)
        }
    }

    componentDidMount() {
        if (this.props.currentUser) {
            this.props.history.push("/chat");
        }
    }

    onChangeField(e, target) {
        e.preventDefault();
        this.setState({
            [target]: e.target.value
        })
    }

    onSubmit(e) {
        e.preventDefault();

        const data = {
            login: this.state.login,
            password: this.state.password
        }

        this.props.login(data);
    }

    onSignUp(e) {
        e.preventDefault();
        this.props.history.push("/sign-up");
    }

    render() {
        const {
            onChangeField,
            onSubmit,
            onSignUp,
            ...rest
        } = this.state;
        const {
            loginErrors
        } = this.props;

        return (
            <LoginForm
                onChangeField={ onChangeField }
                onSubmit={ onSubmit }
                onSignUp={ onSignUp }
                values={ rest }
                fieldsKeys={ fieldsKeys }
                errors={ loginErrors }
            />
        )
    }
}

export default withContextConsumer(UserContext.Consumer)(Login);
