import React, {forwardRef} from "react";

export const withContextConsumer = Consumer => WrappedComponent =>
    forwardRef((props, ref) => (
        <Consumer>{contextState => <WrappedComponent ref={ref} {...contextState} {...props} />}</Consumer>
    ));