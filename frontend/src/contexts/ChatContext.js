import React from "react";
import SocketController from "../network/SocketController";
import * as NetworkUtils from "../network/http/chat";
import { withContextConsumer } from "./utils";
import UserContext from "./UserContext";
import { withRouter } from "react-router-dom";

const ChatContext = React.createContext();

class ChatContextProvider extends React.Component {
    constructor(props) {
        super(props);
        this.socketController = new SocketController();

        this.state = {
            isLoadingRooms: false,
            rooms: [],
            token: "",

            sendMessageError: "",
            createRoomError: "",

            onMessageReceiveCallback: null,
            onActiveMembersUpdateCallback: null,

            prepareConnection: this.prepareConnection.bind(this),
            loadRooms: this.loadRooms.bind(this),
            createRoom: this.createRoom.bind(this),
            sendMessage: this.sendMessage.bind(this),
            getMessages: this.getMessages.bind(this),
            setOnMessageReceiveCallback: this.setOnMessageReceiveCallback.bind(this),
            setOnActiveMembersUpdateCallback: this.setOnActiveMembersUpdateCallback.bind(this),
            disconnect: this.disconnect.bind(this),
            disconnectAll: this.disconnectAll.bind(this)
        }
    }

    setOnMessageReceiveCallback(callback) {
        this.onMessageReceiveCallback = callback
    }

    setOnActiveMembersUpdateCallback(callback) {
        this.onActiveMembersUpdateCallback = callback;
    }

    async createRoom(chatRoomName) {
        await this.setState({
            createRoomError: ""
        })

        const name = chatRoomName.trim();

        if (name === "") {
            return this.setState({
                createRoomError: "Room name is required"
            })
        }

        const response = await NetworkUtils.createChat(name);
        if (!response.hasErrors) {
            const updatedRooms = this.state.rooms;
            updatedRooms.push(response.data.room);
            this.setState({
                rooms: updatedRooms
            })
        } else {
            await this.setState({
                createRoomError: Object.values(response.data)[0]
            })
        }
    }

    async loadRooms() {
        if (!this.props.currentUser)
            return this.props.history.push("/")

        await this.setState({
            isLoadingRooms: true
        });

        const response = await NetworkUtils.getChatList();
        const token = response.data.token;

        if (!response.hasErrors) {
            this.setState({
                rooms: response.data.rooms,
                token
            })
        }

        this.setState({
            isLoadingRooms: false
        });
    }

    async prepareConnection(roomId, onConnect, onError) {
        this.socketController.connect(roomId, this.state.token);
        this.socketController.on(roomId, "connect_error", (data) => onError(data))
        this.socketController.on(roomId, "connect", async () => {
            if (this.state.rooms.filter(room => room.id === roomId).length === 0) {
                await this.loadRooms();
            }
            onConnect(this.state.rooms.filter(room => room.id === roomId)[0]);
        });
        this.socketController.on(roomId, "chat:message", (data) => {
            this.onMessageReceiveCallback
                && this.onMessageReceiveCallback(data);
        });
        this.socketController.on(roomId, "chat:updateActiveUsers", (data) => {
            this.onActiveMembersUpdateCallback
                && this.onActiveMembersUpdateCallback(data)
        })
    }

    disconnect(roomId) {
        this.socketController.disconnect(roomId);
    }

    disconnectAll() {
        this.socketController.disconnectAll();
    }

    async sendMessage(room, message) {
        await this.setState({
            sendMessageError: ""
        });

        if (message.trim() === "") {
            return this.setState({
                sendMessageError: "Message is required"
            })
        }

        this.socketController.emit(
            room.id,
            "chat:sendMessage",
            { text: message.trim(), roomId: room.id },
            (response) => {
                if (response && response.error) {
                    return this.setState({
                        sendMessageError: response.error.content
                    })
                }
            }
        );
    }

    getMessages(room, callback) {
        this.socketController.emit(
            room.id,
            "chat:getMessages",
            { roomId: room.id },
            (response) => {
                callback(response.messages)
            }
        )
    }

    render() {
        return (
            <ChatContext.Provider value={ this.state }>
                { this.props.children }
            </ChatContext.Provider>
        )
    }
}

const CustomChatContext = {
    Provider: withRouter(withContextConsumer(UserContext.Consumer)(ChatContextProvider)),
    Consumer: ChatContext.Consumer
};

export default CustomChatContext