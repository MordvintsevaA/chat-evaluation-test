import React from "react";
import * as NetworkUtils from "../network/http/user";
import { validateLoginData, validateRegistrationData } from "../utils/validation";
import { withRouter } from "react-router-dom";

const UserContext = React.createContext();

class UserContextProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: null,
            registerErrors: {},
            loginErrors: {},
            isLoadingUser: true,

            register: this.register.bind(this),
            login: this.login.bind(this),
            logout: this.logout.bind(this)
        }
    }

    async componentDidMount() {
        const response = await NetworkUtils.getMe()

        if (response.hasErrors) {
            return this.setState({
                isLoadingUser: false,
                currentUser: null
            });
        }

        this.setState({
            isLoadingUser: false,
            currentUser: response.data.user
        })
    }

    async register(data) {
        await this.setState({
            registerErrors: {}
        })

        const validationErrors = validateRegistrationData(data);

        if (Object.keys(validationErrors).length > 0) {
            this.setState({
                registerErrors: validationErrors
            })
            return
        }

        delete data.confirmPassword;

        const response = await NetworkUtils.userRegistration(data);

        if (response.hasErrors) {
            this.setState({
                registerErrors: response.data
            })
        } else {
            await this.setState({
                currentUser: response.data.user
            })

            this.props.history.push('/chat');
        }
    }

    async login(data) {
        await this.setState({
            loginErrors: {}
        })

        const validationErrors = validateLoginData(data);
        if (Object.keys(validationErrors).length > 0) {
            this.setState({
                loginErrors: validationErrors
            })
            return
        }

        const response = await NetworkUtils.login(data);

        if (response.hasErrors) {
            this.setState({
                loginErrors: response.data
            })
        } else {
            await this.setState({
                currentUser: response.data.user
            })

            this.props.history.push('/chat');
        }
    }

    async logout() {
        await NetworkUtils.logout();

        this.setState({
            currentUser: null
        })

        this.props.history.push("/")
    }

    render() {
        return (
            <UserContext.Provider value={ this.state }>
                {
                    !this.state.isLoadingUser && this.props.children
                }
            </UserContext.Provider>
        )
    }
}

const CustomUserContext = {
    Provider: withRouter(UserContextProvider),
    Consumer: UserContext.Consumer
};

export default CustomUserContext