import { io } from "socket.io-client";

class SocketController {
    constructor() {
        this.sockets = {};
    }

    connect(roomId, token) {
        if (this.sockets[roomId]) return;

        const socket = io('wss://mordvintseva-webdev.ru/', {
            reconnectionDelayMax: 10000,
            reconnection: true,
            auth: {
                token: token
            },
            query: {
                roomId
            }
        });
        this.sockets[roomId] = socket;
        socket.connect();
    }

    on(roomId, event, callback) {
        this.sockets[roomId].on(event, (data) => callback && callback(data));
    }

    emit(roomId, event, data, callback) {
        this.sockets[roomId].emit(event, data, callback);
    }

    disconnect(roomId) {
        const socket = this.sockets[roomId];

        socket.disconnect();
        socket.removeAllListeners();
        delete this.sockets[roomId];
    }

    disconnectAll() {
        for (let socket of Object.values(this.sockets)) {
            socket.disconnect();
            socket.removeAllListeners();
        }
        this.sockets = {};
    }
}

export default SocketController
