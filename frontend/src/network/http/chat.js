import NetworkController from "./NetworkController";

const getChatList = async () => {
    return await NetworkController.get("/chats");
}

const createChat = async (name) => {
    return await NetworkController.post(
        "/chats", {
            name
        });
}

export {
    getChatList,
    createChat
}