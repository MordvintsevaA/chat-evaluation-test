import NetworkController from "./NetworkController";

const userRegistration = async ({ login, firstName, lastName, password }) => {
    return await NetworkController.post(
        "/users/sign-up", {
            login,
            firstName,
            lastName,
            password
        }
    );
}

const getMe = async () => {
    return await NetworkController.get("/users");
}

const logout = async () => {
    return await NetworkController.post("/users/logout");
}

const login = async (data) => {
    return await NetworkController.post("/users/login", data);
}

export {
    userRegistration,
    getMe,
    login,
    logout
}