import axios from "axios";

class NetworkController {
    constructor() {
        this.axios = axios.create({
            withCredentials: true,
            baseURL: "https://mordvintseva-webdev.ru/api",
            responseType: "json"
        });
    }

    async post(url, data) {
        try {
            const response = await this.axios({
                url,
                method: "POST",
                data: data
            })

            return {
                hasErrors: response.status !== 200,
                data: response.data
            }
        } catch (e) {
            return {
                hasErrors: true,
                data: e.response?.data || { unknown: "Unknown error, try again later" }
            }
        }
    }

    async get(url) {
        try {
            const response = await this.axios({
                url,
                method: "GET"
            })

            return {
                hasErrors: response.status !== 200,
                data: response.data
            }
        } catch (e) {
            return {
                hasErrors: true,
                data: e.response?.data || { unknown: "Unknown error, try again later" }
            }
        }
    }
}

export default new NetworkController();
