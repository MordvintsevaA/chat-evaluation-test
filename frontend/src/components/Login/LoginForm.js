import React from "react";
import styles from "./style.module.css";
import InformationMessage from "../InformationMessage/InformationMessage";

class LoginForm extends React.Component {
    render() {
        const {
            values,
            fieldsKeys,
            errors,
            onChangeField,
            onSignUp,
            onSubmit
        } = this.props;

        return (
            <div className={ styles.container }>
                <div className={ styles.title }>
                    Login
                </div>
                <div className={ styles.description }>
                    <span>Not registered yet? </span>
                    <input
                        className={ styles.link }
                        type={ "button" }
                        onClick={ onSignUp }
                        value={ "Create an account" }
                    />
                </div>
                <form autoComplete={ "on" }>
                    <input
                        className={ styles.input }
                        type={ "text" }
                        placeholder="Login"
                        value={ values.login }
                        onChange={ (e) => onChangeField(e, fieldsKeys.login) }
                        onKeyPress={ (e) => {
                            if (e.code === "Enter") onSubmit(e)
                        } }
                        autoComplete={ "login" }
                    />
                    { errors.login && <InformationMessage message={ errors.login }/> }
                    <input
                        className={ styles.input }
                        type={ "password" }
                        placeholder="Password"
                        value={ values.password }
                        onChange={ (e) => {
                            onChangeField(e, fieldsKeys.password)
                        } }
                        onKeyPress={ (e) => {
                            if (e.code === "Enter") onSubmit(e)
                        } }
                        autoComplete={ "current-password" }
                    />
                    { errors.password && <InformationMessage message={ errors.password }/> }
                    { errors.error && <InformationMessage message={ errors.error }/> }
                    <input
                        className={ styles.button }
                        type={ "button" }
                        value={ "Sign In" }
                        onClick={ onSubmit }
                    />
                </form>
            </div>
        )
    }
}

export { LoginForm };
