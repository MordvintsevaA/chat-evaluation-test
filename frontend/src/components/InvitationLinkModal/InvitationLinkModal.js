import React from "react";
import Modal from "@material-ui/core/Modal";
import styles from "./style.module.css"
import Link from "../../common/icons/Copy";

class InvitationLinkModal extends React.Component {
    render() {
        const {
            open,
            onClose,
            invitationLink
        } = this.props;

        let message = null;

        const copyLink = (e) => {
            e.preventDefault();
            if (navigator.clipboard) {
                navigator.clipboard.writeText(invitationLink).then(() => {
                    onClose();
                })
            }
        }

        return (
            <Modal
                open={ open }
                onClose={ onClose }
            >
                <div className={ styles.modal_container }>
                    <div className={ styles.modal_inner }>
                        <p>Copy the link and share it to invite people to chat room.</p>
                        <div className={ styles.row }>
                            <input
                                className={ styles.input }
                                value={ invitationLink }
                                readOnly={ true }
                                type={ "text" }
                            />
                            <Link
                                className={ styles.copy_icon }
                                onClick={ copyLink }
                            />
                        </div>
                        { message }
                    </div>
                </div>
            </Modal>
        )
    }
}

export default InvitationLinkModal;
