import styles from "./style.module.css";
import React from "react";

class InformationMessage extends React.Component {
    render() {
        const {
            message,
            success
        } = this.props;

        return (
            <div className={ `${ styles.message } ${ success ? styles.message_success : styles.message_error }` }>
                { message }
            </div>
        )
    }
}

export default InformationMessage;
