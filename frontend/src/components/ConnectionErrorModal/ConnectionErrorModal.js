import React from "react";
import Modal from "@material-ui/core/Modal";
import styles from "./styles.module.css";

class ConnectionErrorModal extends React.Component {
    render() {
        const {
            open,
            onClose,
            errorMessage
        } = this.props;

        return (
            <Modal
                open={ open }
                onClose={ onClose }
            >
                <div className={ styles.modal_container }>
                    <div>
                        {
                            "An error occurred while connecting.\n"
                        }
                    </div>
                    <div>
                        {
                            `Reason: ${ errorMessage }`
                        }
                    </div>
                </div>
            </Modal>
        )
    }
}

export default ConnectionErrorModal;