import React from "react";
import styles from "./style.module.css"
import InformationMessage from "../InformationMessage/InformationMessage";

class RegistrationForm extends React.Component {
    render() {
        const {
            values,
            fieldsKeys,
            errors,
            onChange,
            onSubmit,
            onSignIn
        } = this.props

        return (
            <div className={ styles.container }>
                <div className={ styles.title }>
                    Create account
                </div>
                <div className={ styles.description }>
                    <span>{ "Already have an account? " }</span>
                    <input
                        className={ styles.link }
                        type={ "button" }
                        onClick={ onSignIn }
                        value={ "Sign in" }
                    />
                </div>
                <form autoComplete={ "on" }>
                    <input
                        className={ styles.input }
                        type={ "text" }
                        placeholder="Login"
                        value={ values.login }
                        onChange={ (e) => onChange(e, fieldsKeys.login) }
                        onKeyPress={ (e) => {
                            if (e.code === "Enter") onSubmit(e)
                        } }
                    />
                    { errors[fieldsKeys.login] &&
                        <InformationMessage message={ errors[fieldsKeys.login] }/>
                    }
                    <div className={ styles.row }>
                        <div>
                            <input
                                className={ styles.input_plain_right_border }
                                type={ "text" }
                                placeholder="First name"
                                autoComplete={ "given-name" }
                                value={ values.firstName }
                                onChange={ (e) => onChange(e, fieldsKeys.firstName) }
                                onKeyPress={ (e) => {
                                    if (e.code === "Enter") onSubmit(e)
                                } }
                            />
                            { errors[fieldsKeys.firstName] &&
                                <InformationMessage message={ errors[fieldsKeys.firstName] }/>
                            }
                        </div>
                        <div>
                            <input
                                className={ styles.input_plain_left_border }
                                type={ "text" }
                                placeholder="Last name"
                                autoComplete={ "family-name" }
                                value={ values.lastName }
                                onChange={ (e) => onChange(e, fieldsKeys.lastName) }
                                onKeyPress={ (e) => {
                                    if (e.code === "Enter") onSubmit(e)
                                } }
                            />
                            { errors[fieldsKeys.lastName] &&
                                <InformationMessage message={ errors[fieldsKeys.lastName] }/>
                            }
                        </div>
                    </div>
                    <input
                        className={ styles.input }
                        type={ "password" }
                        placeholder="Password"
                        autoComplete={ "new-password" }
                        value={ values.password }
                        onChange={ (e) => onChange(e, fieldsKeys.password) }
                        onKeyPress={ (e) => {
                            if (e.code === "Enter") onSubmit(e)
                        } }
                    />
                    { errors[fieldsKeys.password] &&
                        <InformationMessage message={ errors[fieldsKeys.password] }/>
                    }
                    <input
                        className={ styles.input }
                        type={ "password" }
                        placeholder="Confirm password"
                        autoComplete={ "new-password" }
                        value={ values.confirmPassword }
                        onChange={ (e) => onChange(e, fieldsKeys.confirmPassword) }
                        onKeyPress={ (e) => {
                            if (e.code === "Enter") onSubmit(e)
                        } }
                    />
                    { errors[fieldsKeys.confirmPassword] &&
                        <InformationMessage message={ errors[fieldsKeys.confirmPassword] }/>
                    }
                    <input
                        className={ styles.button }
                        type={ "button" }
                        value={ "Sign Up" }
                        onClick={ onSubmit }
                    />
                </form>
            </div>
        )
    }
}

export { RegistrationForm };
