import React from "react";
import styles from "./style.module.css";
import Dot from "../../../common/icons/Dot";

class UserItem extends React.Component {
    render() {
        const {
            firstName,
            lastName,
            login,
            online
        } = this.props;

        return (
            <div className={ styles.item_container }>
                <div className={ styles.item_header }>
                    {
                        online && <Dot className={ styles.online_icon }/>
                    }
                    <div className={ styles.item_name }>
                        { [ lastName, firstName ].join(" ") }
                    </div>
                </div>

                <div className={ styles.item_login }>
                    { "@" + login }
                </div>
            </div>
        )
    }
}

export default UserItem;