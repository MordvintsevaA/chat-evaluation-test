import React from "react";
import styles from "./style.module.css";
import UserItem from "./UserItem";
import { Scrollbars } from "react-custom-scrollbars";
import Arrow from "../../../common/icons/Arrow";

class UsersList extends React.Component {
    createRoomList(users, online) {
        return users.map(user => {
            return (
                <UserItem
                    firstName={ user.firstName }
                    lastName={ user.lastName }
                    login={ user.login }
                    key={ user.id }
                    online={ online }
                />
            )
        })
    }

    render() {
        const {
            online,
            offline,
            open,
            onClose
        } = this.props;

        const onlineUsers = this.createRoomList(online, true);
        const offlineUsers = this.createRoomList(offline, false);

        return (
            <div className={ open ? styles.container_active : styles.container_inactive }>
                <div className={ styles.header }>
                    <Arrow
                        className={ styles.arrow_icon }
                        onClick={ onClose }
                    />
                    <div className={ styles.title }>
                        { "Members" }
                    </div>
                </div>
                <Scrollbars
                    autoHide
                    autoHideTimeout={ 500 }
                    autoHideDuration={ 200 }
                >
                    {
                        onlineUsers.length > 0 && (
                            <div className={ styles.subtitle }>
                                { "Online" }
                            </div>
                        )
                    }
                    { onlineUsers }
                    {
                        offlineUsers.length > 0 && (
                            <div className={ styles.subtitle }>
                                { "Offline" }
                            </div>
                        )
                    }
                    { offlineUsers }
                </Scrollbars>
            </div>
        )
    }
}

export default UsersList;