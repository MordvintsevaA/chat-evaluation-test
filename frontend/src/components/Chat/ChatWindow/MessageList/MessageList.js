import React from "react";
import MessageItem from "./MessageItem";
import { formatDate } from "../../../../utils/formatDate";
import { Scrollbars } from "react-custom-scrollbars";

class MessageList extends React.Component {
    constructor(props) {
        super(props);

        this.scrollRef = React.createRef();
        this.previousScrollTop = null;
    }

    componentDidUpdate(prevProps) {
        if (!this.scrollRef.current) return;

        // scroll to bottom on first messages loading
        if (!prevProps.messages.length && this.props.messages.length) {
            this.scrollRef.current.scrollToBottom();
            this.previousScrollTop = this.scrollRef.current.getValues().scrollTop;
            return
        }

        // scroll to bottom on current user new message
        if ((prevProps.messages.length < this.props.messages.length)
            && (this.props.messages[this.props.messages.length - 1].owner.id === this.props.currentUser.id)) {
            this.scrollRef.current.scrollToBottom();
            this.previousScrollTop = this.scrollRef.current.getValues().scrollTop;
            return;
        }

        const { scrollHeight, scrollTop, clientHeight } = this.scrollRef.current.getValues();
        // if bottom is reached - update the previous scroll top value
        if (Math.abs(scrollHeight - (scrollTop + clientHeight)) < 1.0) {
            this.previousScrollTop = scrollTop;
        }

        // scroll to bottom if user is at the end of message list and new message is appeared
        if (prevProps.messages.length < this.props.messages.length
            && (this.props.messages[this.props.messages.length - 1].owner.id !== this.props.currentUser.id)
            && (Math.abs(this.previousScrollTop - scrollTop) < 0.15 * clientHeight)) {
            this.scrollRef.current.scrollToBottom();
            this.previousScrollTop = this.scrollRef.current.getValues().scrollTop;
        }
    }

    componentDidMount() {
        this.scrollRef.current && this.scrollRef.current.scrollToBottom();
    }

    createMessageList(messages) {
        const { currentUser } = this.props;

        messages.sort((a, b) => {
            return (new Date(a.createdAt)).getTime() > (new Date(b.createdAt)).getTime() ? 1 : -1;
        })

        return messages.map(message => {
            return (
                <MessageItem
                    key={ message.id }
                    text={ message.content }
                    date={ formatDate(new Date(message.createdAt)) }
                    name={ currentUser?.id === message.owner.id ? "You"
                        : [ message.owner.firstName, message.owner.lastName ].join(" ") }
                    outgoing={ currentUser?.id === message.owner.id }
                />
            )
        });
    }

    render() {
        const {
            messages
        } = this.props;

        const messageItems = this.createMessageList.call(this, messages);

        return (
            <Scrollbars
                autoHide
                autoHideTimeout={ 500 }
                autoHideDuration={ 200 }
                ref={ this.scrollRef }
            >
                { messageItems }
            </Scrollbars>
        )
    }
}

export default MessageList
