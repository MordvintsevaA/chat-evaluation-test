import React from "react";
import styles from "./styles.module.css";

class MessageItem extends React.Component {
    render() {
        const {
            name,
            date,
            text,
            outgoing
        } = this.props;

        return (
            <div className={ outgoing ? styles.message_item_outgoing : styles.message_item }>
                <div className={ styles.message_item_info }>
                    <div className={ styles.message_item_name }>
                        { name }
                    </div>
                    <div className={ styles.message_item_date }>
                        { date }
                    </div>
                </div>
                <div className={ styles.message_item_text }>
                    { text }
                </div>
            </div>
        )
    }
}

export default MessageItem;
