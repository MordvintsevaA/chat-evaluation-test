import React from "react";
import MessageList from "./MessageList/MessageList";
import styles from "./style.module.css";
import Share from "../../../common/icons/Share";
import Send from "../../../common/icons/Send";
import ErrorMessage from "../../InformationMessage/InformationMessage";
import Arrow from "../../../common/icons/Arrow";
import ThreeDots from "../../../common/icons/ThreeDots";

class ChatWindow extends React.Component {
    render() {
        const {
            show,
            activeChat,
            newMessage,
            messages,
            onMessageChange,
            onSendClick,
            sendMessageError,
            onInviteClick,
            currentUser,
            onRoomDeselect,
            onOpenMembersList
        } = this.props;

        return (
            <div className={ show ? styles.container : styles.container_inactive }>
                <div className={ styles.chat_header }>
                    <div className={ styles.chat_header_room_block }>
                        <Arrow
                            className={ styles.arrow_icon }
                            onClick={ onRoomDeselect }
                        />
                        <div className={ styles.chat_header_title }>
                            { activeChat.name }
                        </div>
                        <div className={ styles.chat_header_item }>
                            <Share
                                className={ styles.share_icon }
                                onClick={ onInviteClick }
                            />
                        </div>
                    </div>
                    <ThreeDots
                        className={ styles.three_dots_icon }
                        onClick={ onOpenMembersList }
                    />
                </div>
                <MessageList
                    messages={ messages }
                    currentUser={ currentUser }
                />
                <div>
                    <div className={ styles.chat_send_message_container }>
                        <input
                            className={ styles.chat_send_message_input }
                            type={ "text" }
                            value={ newMessage }
                            placeholder={ "Enter the message" }
                            onChange={ onMessageChange }
                            onKeyPress={ (e) => {
                                if (e.code === "Enter") onSendClick(e)
                            } }
                        />
                        <Send
                            className={ styles.chat_send_message_button }
                            onClick={ onSendClick }
                        />
                    </div>
                    <ErrorMessage
                        message={ sendMessageError }
                    />
                </div>
            </div>
        )
    }
}

export default ChatWindow;
