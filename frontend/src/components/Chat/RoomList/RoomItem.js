import React from "react";
import styles from "./style.module.css";

class RoomItem extends React.Component {
    render() {
        const {
            name,
            selected,
            onClick
        } = this.props;

        const style = selected ? styles.room_item_selected : styles.room_item;

        return (
            <div
                className={ style }
                onClick={ onClick }>
                { name }
            </div>
        )
    }
}

export default RoomItem;
