import React from "react";
import styles from "./style.module.css";
import RoomItem from "./RoomItem";
import InformationMessage from "../../InformationMessage/InformationMessage";
import { Scrollbars } from "react-custom-scrollbars";

class RoomList extends React.Component {
    createRoomList(rooms) {
        const {
            onRoomSelect,
            activeChatRoomId
        } = this.props;

        return rooms.map(room => {
            return (
                <RoomItem
                    name={ room.name }
                    key={ room.id }
                    onClick={ (e) => onRoomSelect(e, room.id) }
                    selected={ room.id === activeChatRoomId }
                />
            )
        })
    }

    render() {
        const {
            rooms,
            newChatRoomName,
            onChatRoomNameChange,
            onCreateChatClick,
            createRoomError,
            activeChatRoomId,
            onLogoutClick
        } = this.props;

        const roomItems = this.createRoomList(rooms);

        return (
            <div className={ activeChatRoomId ? styles.container : styles.container_inactive }>
                <div className={ styles.row }>
                    <input
                        className={ styles.input }
                        type={ "text" }
                        value={ newChatRoomName }
                        placeholder={ "Enter new room name" }
                        onChange={ onChatRoomNameChange }
                    />
                    <input
                        className={ styles.button }
                        type={ "button" }
                        onClick={ onCreateChatClick }
                        value={ "Create" }
                    />
                </div>
                { createRoomError && <InformationMessage message={ createRoomError }/> }
                <Scrollbars
                    autoHide
                    autoHideTimeout={ 500 }
                    autoHideDuration={ 200 }
                >
                    <div>
                        { roomItems }
                    </div>
                </Scrollbars>
                <input
                    className={ styles.logout_button }
                    type={ "button" }
                    value={ "Logout" }
                    onClick={ onLogoutClick }
                />
            </div>
        )
    }
}

export default RoomList;