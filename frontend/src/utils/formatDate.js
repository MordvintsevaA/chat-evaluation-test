const formatDate = (date) => {
    const month = ("0" + (date.getMonth() + 1)).slice(-2);
    const day = ("0" + (date.getDate())).slice(-2);
    const year = date.getFullYear();

    const hours = ("0" + (date.getHours())).slice(-2);
    const minutes = ("0" + (date.getMinutes())).slice(-2);
    return [ day, month, year ].join(".") + " " + [ hours, minutes ].join(":");
}

export {
    formatDate
}