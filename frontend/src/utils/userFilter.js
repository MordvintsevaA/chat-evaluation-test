function filterOfflineUsers(users, onlineUsers) {
    const online = new Set(onlineUsers.map(user => user.id));
    return users.filter(user => !online.has(user.id));
}

export {
    filterOfflineUsers
};