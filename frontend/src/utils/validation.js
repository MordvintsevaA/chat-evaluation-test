const fieldsKeys = {
    login: "login",
    firstName: "firstName",
    lastName: "lastName",
    password: "password",
    confirmPassword: "confirmPassword"
}

const MIN_LOGIN_LENGTH = 6;
const MAX_LOGIN_LENGTH = 40;
const MIN_FIRST_NAME_LENGTH = 2;
const MAX_FIRST_NAME_LENGTH = 100;
const MIN_SECOND_NAME_LENGTH = 2;
const MAX_SECOND_NAME_LENGTH = 100;
const MIN_PASSWORD_LENGTH = 6;

const validateRegistrationData = (data) => {
    const validationErrors = {};

    if (data.login.length < MIN_LOGIN_LENGTH
        || data.login.length > MAX_LOGIN_LENGTH) {
        validationErrors[fieldsKeys.login] = `
            Login must contain ${ MIN_LOGIN_LENGTH }-${ MAX_LOGIN_LENGTH } characters
        `;
    }
    if (data.firstName.length < MIN_FIRST_NAME_LENGTH
        || data.firstName.length > MAX_FIRST_NAME_LENGTH) {
        validationErrors[fieldsKeys.firstName] = `
            First name must contain ${ MIN_FIRST_NAME_LENGTH }-${ MAX_SECOND_NAME_LENGTH } characters
        `;
    }
    if (data.lastName.length < MIN_SECOND_NAME_LENGTH
        || data.lastName.length > MAX_SECOND_NAME_LENGTH) {
        validationErrors[fieldsKeys.lastName] = `
            Last name must contain ${ MIN_SECOND_NAME_LENGTH }-${ MAX_SECOND_NAME_LENGTH } characters
        `;
    }
    if (data.password === "") {
        validationErrors[fieldsKeys.password] = "Password is required";
    }
    if (data.password.length < MIN_PASSWORD_LENGTH) {
        validationErrors[fieldsKeys.password] = `Password's length must be greater than ${ MIN_PASSWORD_LENGTH }`;
    }
    if (data.password !== "" && (data.password !== data.confirmPassword)) {
        validationErrors[fieldsKeys.confirmPassword] = "Passwords must match"
    }

    return validationErrors;
}

const validateLoginData = (data) => {
    const validationErrors = {};

    if (data.login.length <= 0) {
        validationErrors[fieldsKeys.login] = `
            Login is required
        `;
    }
    if (data.password === "") {
        validationErrors[fieldsKeys.password] = "Password is required";
    }

    return validationErrors;
}

export {
    validateRegistrationData,
    validateLoginData
}