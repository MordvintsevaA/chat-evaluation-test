# Chat Evaluation Test

This project is a text chat implementation. The following functionality is available:

+ User registration and authorization
+ Create chat rooms
+ Chat with people in the chat room
+ Invite people to chat room by link
+ See who is connected to the chat room right now

You can try the [demo](https://mordvintseva-webdev.ru).

Technology Stack:

+ Node.js
+ React.js
+ Express
+ Mongoose
+ Socket.io

Made by Mordvintseva Alena

## Authorization & Authentication
To use the application, user must register in order to save the history of messages and protect it from strangers.

User authentication is implemented with JWT and Cookies.

## Rooms & Messages

After authorization, the user can create a chat room and invite other users to start chatting.

> To join the chat room by the link user must be authorized

The user can see the list of users connected to the room. Online members list displays the users connected to the room - not entirely to the application.

## Adaptation
The application has the adaptive design. The view will change based on the width and height of the viewport.