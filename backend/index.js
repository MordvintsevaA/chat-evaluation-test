require("dotenv").config();
const Engine = require("./engine");

(async () => {
    const engine = new Engine();
    await engine.connectDB();
    await engine.start();
})()
