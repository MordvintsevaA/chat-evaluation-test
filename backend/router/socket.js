const { socketAuthMiddleware, socketRoomMiddleware } = require("./middleware");
const ChatApi = require("../api/socket/chat");

class SocketRouter {
    constructor(io) {
        this.io = io;
        this.applyMiddlewares();

        this.io.on("connection", this.handleConnection.bind(this))
    }

    applyMiddlewares() {
        this.io.use(socketAuthMiddleware);
        this.io.use(socketRoomMiddleware);
    }

    async handleConnection(socket) {
        const roomId = socket.handshake.query["roomId"];

        socket.join(roomId);

        socket.on("chat:sendMessage", (data, callback) => ChatApi.createMessage(data, socket, this.io, callback));
        socket.on("chat:getMessages", (data, callback) => ChatApi.getMessageList(data, socket, this.io, callback));
        socket.on("disconnect", () => ChatApi.disconnect(roomId, socket, this.io));

        await ChatApi.connect(roomId, socket, this.io);
    }
}

module.exports = SocketRouter
