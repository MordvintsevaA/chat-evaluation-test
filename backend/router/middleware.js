const jwt = require("jsonwebtoken");
const UserUsecases = require("../models/user/usecases");
const RoomUsecases = require("../models/room/usecases");

const socketAuthMiddleware = (socket, next) => {
    const token = socket.handshake.auth.token;

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, async (err, data) => {
        if (err) {
            return next(new Error("Unauthorized"));
        }

        try {
            socket.handshake.query.user = (await UserUsecases.getUserById(data.id)).toDTO();
            return next();
        } catch (e) {
            if  (e === UserUsecases.ERROR_INVALID_CREDENTIALS) {
                return next(new Error("Unauthorized"));
            }
            console.log(e)
            return next(new Error("Unknown error, try again later"));
        }
    });
}

const socketRoomMiddleware = async (socket, next) => {
    const roomId = socket.handshake.query.roomId;

    try {
        await RoomUsecases.getRoomById(roomId);
        return next();
    } catch (err) {
        if (err === RoomUsecases.ERROR_ROOM_NOT_FOUND) {
            return next(new Error("Room not found"));
        }
        console.log(err);
        return next(err.message);
    }
}

module.exports = {
    socketAuthMiddleware,
    socketRoomMiddleware
}
