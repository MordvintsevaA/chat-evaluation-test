const cors = require("cors");
const cookieParser = require("cookie-parser")
const express = require("express");
const addUsersRoutes = require("./routes/users");
const addChatRoutes = require("./routes/chats");

class Router {
    constructor(app) {
        this.app = app;
    }

    configure() {
        this.applyMiddlewares();
        addUsersRoutes(this.app);
        addChatRoutes(this.app);
    }

    applyMiddlewares() {
        this.app.use(cors({
            credentials: true,
            origin: process.env.ALLOWED_ORIGIN
        }));
        this.app.use(express.json());
        this.app.use(cookieParser());
        this.app.use(express.urlencoded({
            extended: true
        }));
    }
}

module.exports = Router;
