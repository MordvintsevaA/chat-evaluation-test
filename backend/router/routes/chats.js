const { authenticateJWT } = require("../../utils/jwt");
const { getUserRoomList, createRoom } = require("../../api/http/rooms");

module.exports = (app) => {
    app.get("/api/chats", authenticateJWT, getUserRoomList);
    app.post("/api/chats", authenticateJWT, createRoom);
}
