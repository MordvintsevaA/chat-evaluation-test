const {authenticateJWT} = require("../../utils/jwt");
const users = require("../../api/http/users.js");

module.exports = (app) => {
    app.get("/api/users", authenticateJWT, users.getMe);
    app.post("/api/users/login", users.login);
    app.post("/api/users/logout", authenticateJWT, users.logout);
    app.post("/api/users/sign-up", users.createUser);
}
