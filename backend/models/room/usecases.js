const RoomModel = require("./Room");
const UserModel = require("../user/User");
const { ValidationError } = require("../../utils/mongoose");

const ERROR_USER_NOT_FOUND = new Error("ERROR_USER_NOT_FOUND");
const ERROR_ROOM_NOT_FOUND = new Error("ERROR_ROOM_NOT_FOUND");

const connectToRoom = async (userId, roomId) => {
    const user = await UserModel.findById(userId);

    if (!user) {
        throw ERROR_USER_NOT_FOUND;
    }

    const room = RoomModel.findById(roomId);

    if (!room) {
        throw ERROR_ROOM_NOT_FOUND;
    }

    room.users.push(user._id);
    await room.save();
    return RoomModel.findById(room._id).populate("activeUsers users")
}

const getUserRoomList = async ({ _id }) => {
    const rooms = await RoomModel.find({ users: _id })

    return Promise.all(rooms.map(async (room) => {
        return RoomModel.findById(room._id).populate("activeUsers users");
    }))
}

const createRoom = async (userId, chatName) => {
    const user = UserModel.findById(userId);

    if (!user) {
        throw ERROR_USER_NOT_FOUND;
    }

    const chat = new RoomModel({
        name: chatName.trim(),
        users: [
            userId
        ]
    });

    let errors = chat.validateSync();

    if (errors) {
        throw new ValidationError(errors);
    }

    await chat.save();
    return RoomModel.findById(chat._id).populate("activeUsers users")
}

const getRoomById = async (id) => {
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
        throw ERROR_ROOM_NOT_FOUND;
    }
    const room = await RoomModel.findOne({ _id: id });
    if (!room) throw ERROR_ROOM_NOT_FOUND;
    return room;
}

const enterRoom = async (roomId, userId) => {
    const room = await RoomModel.findById(roomId);
    const user = await UserModel.findById(userId);

    if ((!room) || (!user)) return;

    if (room.users.filter(item => item.equals(user._id)).length === 0) {
        room.users.push(user._id);
    }

    if (room.activeUsers.filter(item => item.equals(user._id)).length === 0) {
        room.activeUsers.push(user._id);
    }

    await room.save();
    return RoomModel.findById(room._id).populate("activeUsers users");
}

const leaveRoom = async (roomId, userId) => {
    const room = await RoomModel.findById(roomId);
    const user = await UserModel.findById(userId);

    if ((!room) || (!user)) return;

    room.activeUsers = room.activeUsers.filter((item) => {
        return !item.equals(user._id)
    });
    await room.save();
    return RoomModel.findById(room._id).populate("activeUsers users");
}

module.exports = {
    getUserRoomList,
    createRoom,
    connectToRoom,
    getRoomById,
    enterRoom,
    leaveRoom,
    ERROR_USER_NOT_FOUND,
    ERROR_ROOM_NOT_FOUND
}
