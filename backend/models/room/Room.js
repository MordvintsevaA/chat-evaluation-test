const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const Room = mongoose.Schema({
    name: {
        type: String,
        required: [ true, "Room name is required" ],
        minValue: [ 1, "Room name must contain 1-100 characters" ],
        maxValue: [ 100, "Room name must contain 1-100 characters" ]
    },
    users: [
        {
            type: ObjectId,
            ref: "User",
            required: true
        }
    ],
    activeUsers: [
        {
            type: ObjectId,
            ref: "User"
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now
    }
})

Room.methods.toDTO = function () {
    return {
        id: this._id,
        name: this.name,
        users: this.users,
        activeUsers: this.activeUsers,
        createdAt: this.createdAt
    }
}

module.exports = mongoose.model("Room", Room);
