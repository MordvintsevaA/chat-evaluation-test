const MessageModel = require("./Message");
const { ValidationError } = require("../../utils/mongoose");
const RoomModel = require("../../models/room/Room");

const ERROR_ACCESS_DENIED = new Error("ERROR_ACCESS_DENIED");
const ERROR_ROOM_NOT_FOUND = new Error("ERROR_ROOM_NOT_FOUND");

const createMessage = async (roomId, ownerId, text) => {
    const room = await RoomModel.findById(roomId);
    if (!room) {
        throw ERROR_ROOM_NOT_FOUND;
    }

    if (!room.users.includes(ownerId)) {
        throw ERROR_ACCESS_DENIED;
    }

    const message = new MessageModel({
        room: roomId,
        owner: ownerId,
        content: text.trim()
    })

    let errors = message.validateSync();

    if (errors) {
        throw new ValidationError(errors);
    }

    await message.save();
    return MessageModel.findById(message._id).populate("owner");
}

const getMessageList = async (roomId, userId) => {
    const room = await RoomModel.findById(roomId);
    if (!room) {
        throw ERROR_ROOM_NOT_FOUND;
    }
    if (!room.users.includes(userId)) {
        throw ERROR_ACCESS_DENIED;
    }

    return MessageModel
        .find({
            room: roomId
        }).populate("owner");
}

module.exports = {
    createMessage,
    getMessageList,
    ERROR_ACCESS_DENIED,
    ERROR_ROOM_NOT_FOUND
}
