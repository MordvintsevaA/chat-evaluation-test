const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const Message = mongoose.Schema({
    room: {
        type: ObjectId,
        ref: "Room",
        required: [ true, "Room id is required" ],
    },
    owner: {
        type: ObjectId,
        ref: "User",
        required: [ true, "User id is required" ],
    },
    content: {
        type: String,
        required: [ true, "Message is required" ],
        minValue: [ 1, "Message is required" ]
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

Message.methods.toDTO = function () {
    return {
        id: this._id,
        owner: this.owner,
        room: this.room,
        content: this.content,
        createdAt: this.createdAt
    }
}

module.exports = mongoose.model("Message", Message);
