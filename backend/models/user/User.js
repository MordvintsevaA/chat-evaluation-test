const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const User = mongoose.Schema({
    login: {
        type: String,
        unique: true,
        required: [ true, "Login is required" ],
        minLength: [ 6, 'Login must contain 6-40 characters' ],
        maxLength: [ 40, 'Login must contain 6-40 characters' ]
    },
    firstName: {
        type: String,
        required: [ true, "First name is required" ],
        minLength: [ 2, "First name must contain 2-100 characters" ],
        maxLength: [ 100, "First name must contain 2-100 characters" ]
    },
    lastName: {
        type: String,
        required: [ true, "Last name is required" ],
        minLength: [ 2, "Last name must contain 2-100 characters" ],
        maxLength: [ 100, "Last name must contain 2-100 characters" ]
    },
    password: {
        type: String,
        required: [ true, "Password is required" ],
        minLength: [ 6, "Password's length must be greater than 6" ]
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

User.methods.toDTO = function () {
    return {
        id: this._id,
        login: this.login,
        firstName: this.firstName,
        lastName: this.lastName
    }
}

User.methods.setupDefaults = function () {
    let firstName = this.firstName.trim();
    let lastName = this.lastName.trim();
    firstName = firstName.charAt(0).toUpperCase() + firstName.slice(1);
    lastName = lastName.charAt(0).toUpperCase() + lastName.slice(1);

    this.firstName = firstName;
    this.lastName = lastName;
}

User.methods.setupPassword = function (password) {
    const salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(password, salt);
}

User.methods.isValidPassword = function (password) {
    return bcrypt.compareSync(password, this.password)
}

module.exports = mongoose.model("User", User);
