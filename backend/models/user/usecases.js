const UserModel = require("./User");
const { isDuplicateError, ValidationError } = require("../../utils/mongoose");

const ERROR_DUPLICATE_USER = new Error("ERROR_DUPLICATE_USER");
const ERROR_USER_NOT_FOUND = new Error("ERROR_USER_NOT_FOUND");

const createUser = async ({ login, firstName, lastName, password }) => {
    try {
        let user = new UserModel({
            login: login.trim(),
            firstName: firstName.trim(),
            lastName: lastName.trim(),
            password: password.trim()
        });

        let errors = user.validateSync();

        if (errors) {
            throw new ValidationError(errors);
        }

        user.setupDefaults();
        user.setupPassword(password);
        await user.save();

        return user.toDTO()
    } catch (e) {
        if (isDuplicateError(e, "login")) {
            throw ERROR_DUPLICATE_USER;
        }
        throw e;
    }
}

const getUserById = async (id) => {
    const user = await UserModel.findById(id);
    if (!user) throw ERROR_USER_NOT_FOUND;
    return user;
}

const login = async ({ login, password }) => {
    const user = await UserModel.findOne({ login });

    if (!user) {
        throw ERROR_USER_NOT_FOUND;
    }

    if (!user.isValidPassword(password)) {
        throw ERROR_USER_NOT_FOUND;
    }

    return user.toDTO();
}

module.exports = {
    createUser,
    getUserById,
    login,
    ERROR_DUPLICATE_USER,
    ERROR_USER_NOT_FOUND
}
