const jwt = require("jsonwebtoken");
const UserUsecases = require("../models/user/usecases");

const generateJWT = (payload) => {
    return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET)
}

const authenticateJWT = async (req, res, next) => {
    const token = req.cookies && req.cookies.token;
    if (!token) {
        res.statusCode = 401; // UNAUTHORIZED
        res.send({ auth: "Unauthorized" })
        return
    }

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, async (err, data) => {
        if (err) {
            res.statusCode = 403; // FORBIDDEN
            res.send({ auth: "Unauthorized" });
            return
        }

        try {
            req.currentUser = await UserUsecases.getUserById(data.id);
            req.token = token;
            next();
        } catch (e) {
            if  (e === UserUsecases.ERROR_INVALID_CREDENTIALS) {
                res.statusCode = 403;
                res.send({ auth: "Unauthorized" });
            }
            console.log(e)
            res.statusCode = 500;
            res.send({ error: "Unknown error, try again later" });
        }
    });
}

module.exports = {
    generateJWT,
    authenticateJWT
}
