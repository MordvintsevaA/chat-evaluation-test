class ValidationError extends Error {
    constructor(errors) {
        super("Validation Error has occurred");
        this.name = "ValidationError";
        this.fields = validationErrorsTransformation(errors);
    }
}

const isDuplicateError = (err, fieldName) => {
    return ((err.code === 11000) && (err.keyValue[fieldName]))
}

const validationErrorsTransformation = (errors) => {
    let transformedErrors = {};

    for (const [field, error] of Object.entries(errors.errors)) {
        console.log(error.properties)
        transformedErrors[field] = error.properties.message;
    }

    return transformedErrors;
}

module.exports = {
    ValidationError,
    isDuplicateError
}
