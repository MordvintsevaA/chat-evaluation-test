const MessageUsecases = require("../../models/message/usecases");
const RoomUsecases = require("../../models/room/usecases");

const createMessage = async (data, socket, io, callback) => {
    try {
        const user = socket.handshake.query.user;
        const {
            roomId,
            text
        } = data;

        const message = await MessageUsecases.createMessage(roomId, user.id, text);

        const dto = message.toDTO();
        dto.owner = dto.owner.toDTO();
        io.to(roomId).emit("chat:message", dto);
        callback && callback();
    } catch (e) {
        if (e === MessageUsecases.ERROR_ACCESS_DENIED) {
            return callback && callback({
                error: "Access denied"
            });
        }
        if (e === MessageUsecases.ERROR_ROOM_NOT_FOUND) {
            return callback && callback({
                error: "Invalid room"
            })
        }
        if (e.name === "ValidationError") {
            return callback && callback({
                error: e.fields
            });
        }

        callback && callback({
            error: "Unknown error, try again later"
        });
        console.log(e);
    }
}

const getMessageList = async (data, socket, io, callback) => {
    try {
        const user = socket.handshake.query.user;
        const {
            roomId
        } = data;

        const messages = await MessageUsecases.getMessageList(roomId, user.id);
        callback && callback({
            messages: messages.map(message => {
                const dto = message.toDTO();
                dto.owner = dto.owner.toDTO();
                return dto;
            })
        })
    } catch (e) {
        callback && callback({
            error: "Unknown error, try again later"
        });
        console.log(e);
    }
}

const connect = async (roomId, socket, io) => {
    const user = socket.handshake.query.user;
    const activeUsers = (await RoomUsecases.enterRoom(roomId, user.id)).activeUsers;

    io.to(roomId).emit(
        "chat:updateActiveUsers",
        activeUsers.map(user => user.toDTO())
    )
}

const disconnect = async (roomId, socket, io) => {
    const user = socket.handshake.query.user;
    const activeUsers = (await RoomUsecases.leaveRoom(roomId, user.id)).activeUsers;

    io.to(roomId).emit(
        "chat:updateActiveUsers",
        activeUsers.map(user => user.toDTO())
    )
    socket.leave(roomId);
}

module.exports = {
    createMessage,
    getMessageList,
    connect,
    disconnect
}
