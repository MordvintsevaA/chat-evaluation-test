const RoomUsecases = require("../../models/room/usecases");

const getUserRoomList = async (req, res) => {
    try {
        const user = req.currentUser;

        const rooms = await RoomUsecases.getUserRoomList(user);

        const dto = rooms.map(room => {
            const usersDTO = room.users.map(user => user.toDTO());
            const activeUsersDTO = room.activeUsers.map(user => user.toDTO());
            const roomDTO = room.toDTO();
            return {
                ...roomDTO,
                users: usersDTO,
                activeUsers: activeUsersDTO
            }
        })

        res.statusCode = 200;
        res.send({
            rooms: dto,
            token: req.token
        });
    } catch (e) {
        res.statusCode = 500;
        res.send({ error: "Unknown error, try again later" });
        console.log(e);
    }
}

const createRoom = async (req, res) => {
    try {
        const user = req.currentUser;

        const room = await RoomUsecases.createRoom(user.id, req.body.name);

        const usersDTO = room.users.map(user => user.toDTO());
        const activeUsersDTO = room.activeUsers.map(user => user.toDTO());
        const roomDTO = room.toDTO();

        res.statudCode = 200;
        res.send({
            room: {
                ...roomDTO,
                users: usersDTO,
                activeUsers: activeUsersDTO
            },
            token: req.token
        });
    } catch (e) {
        if (e === RoomUsecases.ERROR_USER_NOT_FOUND) {
            res.statusCode = 400; // BAD REQUEST
            res.send({ user: "Invalid user" });
            return;
        }
        if (e.name === "ValidationError") {
            res.statusCode = 400; // BAD REQUEST
            res.send(e.fields);
            return;
        }
        res.statusCode = 500;
        res.send({ error: "Unknown error, try again later" });
        console.log(e);
    }
}

module.exports = {
    getUserRoomList,
    createRoom
}
