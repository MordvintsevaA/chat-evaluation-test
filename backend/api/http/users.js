const UserUsecases = require("../../models/user/usecases");
const { generateJWT } = require("../../utils/jwt");

const createUser = async (req, res) => {
    try {
        const user = await UserUsecases.createUser(req.body);

        const token = generateJWT({
            id: user.id,
            login: user.login
        });

        res.cookie("token", token, { httpOnly: true });
        res.statusCode = 200;
        res.send({ user });
    } catch (e) {
        if (e === UserUsecases.ERROR_DUPLICATE_USER) {
            res.statusCode = 409; // CONFLICT
            res.send({ login: "This login already exists" });
            return
        }
        if (e.name === "ValidationError") {
            res.statusCode = 400; // BAD REQUEST
            res.send(e.fields);
            return;
        }
        console.log(e)
        res.statusCode = 500;
        res.send({ error: "Unknown error, try again later" });
    }
}

const getMe = async (req, res) => {
    const user = await UserUsecases.getUserById(req.currentUser.id);

    if (!user) {
        res.statusCode = 403 // FORBIDDEN
        res.send();
        return
    }

    res.statusCode = 200;
    res.send({
        user: req.currentUser.toDTO()
    });
}

const logout = (req, res) => {
    res.statusCode = 200;
    res.clearCookie("token");
    res.send();
}

const login = async (req, res) => {
    try {
        const user = await UserUsecases.login(req.body);

        const token = generateJWT({
            id: user.id,
            login: user.login
        });

        res.cookie("token", token, { httpOnly: true });
        res.statusCode = 200;
        res.send({ user });
    } catch (e) {
        if (e === UserUsecases.ERROR_USER_NOT_FOUND) {
            res.statusCode = 400 // BAD REQUEST
            res.send({ error: "Invalid login or password" });
        }

        console.log(e)
        res.statusCode = 500;
        res.send({ error: "Unknown error, try again later" });
    }
}

module.exports = {
    createUser,
    getMe,
    login,
    logout
}
