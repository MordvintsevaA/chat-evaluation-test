const express = require("express");
const http = require("http");
const socket = require("socket.io");
const HttpRouter = require("../router/http");
const SocketRouter = require("../router/socket");
const mongoose = require("mongoose");

class Engine {
    constructor() {
        this.app = express();
        this.server = http.createServer(this.app);
        this.io = socket(this.server, { cors: { origin: process.env.ALLOWED_ORIGIN } });
        this.router = new HttpRouter(this.app);
        this.socketRouter = new SocketRouter(this.io);
    }

    async start() {
        this.router.configure();
        await this.server.listen(process.env.PORT, (err) => {
            if (err) throw Error(err);
            console.log("Server has started");
        })
    }

    async connectDB() {
        try {
            await mongoose.connect(
                process.env.MONGO_URL,
                {
                    useNewUrlParser: true,
                    useUnifiedTopology: true
                }
            );
            this.db = mongoose.connection;
            console.log("Successfully connected to db");
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = Engine;
